#! /usr/bin/env python

import os
import re
import sys
import tempfile
from getopt import getopt, GetoptError

gb_home = "/home/gb"

def usage():
    """Prints out the usage"""
    print """
Usage: %s -c <property-file>

-c    talend job configuration file
""" % (sys.argv[0])

def get_options():
    """Parses command line options.

@return: **options
"""
    # Required options.
    required = ["c"]

    # Get command line options.
    try:
        option_string = ''.join(map(lambda x: x + ":", required))
        options, [] = getopt(sys.argv[1:], option_string)
    except GetoptError:
        usage()
        exit(1)

    # Convert options to dict and validate.
    result = dict()
    for option in options:
        (opt, value) = option
        opt = opt[1:]
        if (opt in required):
            result[opt] = value
        else:
            usage()
            exit(1)

    # Verify that all mandatory parameters are provided.
    for option in required:
        if not result.has_key(option):
            usage()
            exit(1)

    return result

def parse_config_file(filename):
    """Parse configuration file and return a hash"""
    result = dict()
    file = open(filename, "rb")
    while True:
        line = file.readline()
        if not line:
            break
        line = line.rstrip("\r\n")
        if line != "" and line[0] != "#":
            [key, value] = line.split("=")
            result[key] = value
    return result

def combine_war(war_file):
    """Builds a new war with configuration"""
    # Copy configuration file.
    tmpdir = tempfile.mkdtemp()
    os.mkdir(tmpdir + "/WEB-INF")
    classdir = tmpdir + "/WEB-INF/classes"
    os.mkdir(classdir)
    libdir = tmpdir + "/WEB-INF/lib"
    os.mkdir(libdir)
    command = "cp %s/conf/gb-hourly-jobs-test-2/job.properties %s" \
                % (gb_home, classdir)
    print "Executing: " + command
    result = os.system(command)
    if result != 0:
        print "Error: " + command + " failed"
        sys.exit(1)

    command = "jar -uf %s -C %s WEB-INF/classes/job.properties" \
                % (war_file, tmpdir)
    print "Executing: " + command
    result = os.system(command)
    if result != 0:
        print "Error: " + command + " failed"
        sys.exit(1)

    command = "cp %s/conf/gb-hourly-jobs-test-2/log4j.properties %s" \
                % (gb_home, classdir)
    print "Executing: " + command
    result = os.system(command)
    if result != 0:
        print "Error: " + command + " failed"
        sys.exit(1)

    command = "jar -uf %s -C %s WEB-INF/classes/log4j.properties" \
                % (war_file, tmpdir)
    print "Executing: " + command
    result = os.system(command)
    if result != 0:
        print "Error: " + command + " failed"
        sys.exit(1)

    command = "cp %s/lib/cred-res-base/cred-res-base.jar %s" \
                % (gb_home, libdir)
    print "Executing: " + command
    result = os.system(command)
    if result != 0:
        print "Error: " + command + " failed"
        sys.exit(1)

   # command = "cp /usr/share/tomcat7/webapps/gb-hourly-jobs-test-2/WEB-INF/lib/cred-res-base.jar %s" \
    #            % (libdir)
    #print "Executing: " + command
    #result = os.system(command)
    #if result != 0:
     #   print "Error: " + command + " failed"
      #  sys.exit(1)

    command = "jar -uf %s -C %s WEB-INF/lib/cred-res-base.jar" \
              % (war_file, tmpdir)
    print "Executing: " + command
    result = os.system(command)
    if result != 0:
        print "Error: " + command + " failed"
        sys.exit(1)

    os.system("rm -rf " + tmpdir)

def main():
    """Main function"""
    options = get_options()
    config = parse_config_file(options["c"])

    server = config["application.server"]
    port = config["application.server.http.port"]
    uri = config["application.deployment.tomcat.manager.url"]
    login = config["application.deployment.tomcat.manager.login"]
    password = config["application.deployment.tomcat.manager.password"]
    context = config["application.context.path"]
    job_register_url = config["async_registration_url"]
    job_unregister_url = config["async_unregister_url"]

    war_file = gb_home + "/lib/gb-hourly-jobs-test-2/gb-hourly-jobs-test-2.war"

    combine_war(war_file)

    print "Installing war"
    request_uri = "http://%s:%s@%s:%s%s/deploy?path=%s&update=true" \
                    % (login, password, server, port, uri, context)
    curl_command = "/usr/bin/curl -s -X PUT --upload-file %s '%s'" \
                    % (war_file, request_uri)
    print "Executing: " + curl_command
    result = os.system(curl_command)
    if result != 0:
        print "Error: " + curl_command + " failed"
        sys.exit(1)

    # Unregister and register job.
    os.system("curl -v -X DELETE '" + job_unregister_url + "'");
    os.system("curl -v -X POST -H 'Content-Type: application/json' '" + job_register_url + \
                    "' -d @/home/gb/conf/gb-hourly-jobs-test-2/schedule.json");

    sys.exit(0)

if __name__ == "__main__":
    main()