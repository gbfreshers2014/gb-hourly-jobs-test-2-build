/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.talend.jobs.bi.j2ee;

import com.gwynniebee.async.job.helper.AsyncJobRestletServlet;
import com.gwynniebee.talend.jobs.bi.BIJob;
import com.gwynniebee.talend.jobs.bi.config.BIConfig;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import java.io.IOException;

/**
 * @author Prashant TR Rao
 */
public class RestfulServlet extends AsyncJobRestletServlet {

    /**
     * Logger.
     */
    private static Logger log = Logger.getLogger(RestfulServlet.class);

    /**
     * Number of threads for async job.
     */
    private static final int NUM_THREADS = 3;

    /**
     * Constructor.
     */
    public RestfulServlet() {
        super(NUM_THREADS, "", BIJob.class);
    }

    /**
     * Initialize servlet.
     * @throws javax.servlet.ServletException ServletException.
     */
    @Override
    public void init() throws ServletException {
        try {
            BIConfig.init();
        } catch (IOException e) {
            System.out.println("Failed to initialize");
            e.printStackTrace();
        }
        super.init();
    }

    /**
     * Destroy servlet.
     */
    @Override
    public void destroy() {
        super.destroy();
    }
}
