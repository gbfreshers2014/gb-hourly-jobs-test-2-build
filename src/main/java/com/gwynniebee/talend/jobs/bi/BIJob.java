/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.talend.jobs.bi;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.gwynniebee.talend.jobs.TalendJobResourceBase;
import com.gwynniebee.talend.jobs.bi.config.BIConfig;
import com.gwynniebee.talend.objects.TalendProjectJob;

/**
 * @author Prashant TR Rao
 */
public class BIJob extends TalendJobResourceBase {
    /**
     * Pipeline Name.
     */
    private static final String PIPELINE_NAME = "gb";

    /**
     * Estimated time to complete.
     */
    private static final int ESTIMATED_COMPLETION_TIME = 3600;

    /**
     * TODO: This should always be false for release.
     */
    private static final boolean QUICK_MODE = false;

    /**
     * Constructor.
     */
    public BIJob() {
        super();

        Vector<String> dataStatisticsStart = new Vector<String>();
        dataStatisticsStart.add("populateTableStatistics");

        Vector<String> retrieveDumpJobsVector = new Vector<String>();
        // retrieve dumps start.
        retrieveDumpJobsVector.add("retrieveProductDumpAndSizeMapSnapshots");
        if (!QUICK_MODE) {
            retrieveDumpJobsVector.add("retrieveIncrementalTransactionDump");
            retrieveDumpJobsVector.add("retrieveNewInventoryDump");
            retrieveDumpJobsVector.add("retrieveSoldAndReturnVendorDump");
        }
        // retrieve dumps end.

        Vector<String> populateDimesionsJobsVector = new Vector<String>();
        populateDimesionsJobsVector.add("populateDimensionProductType");
        if (!QUICK_MODE) {
            populateDimesionsJobsVector.add("populateDimensionLocationType");
            populateDimesionsJobsVector.add("populateDimensionGarmentCare");
            populateDimesionsJobsVector.add("populateDimensionRecommendation");
            populateDimesionsJobsVector.add("populateDimensionStore");
            populateDimesionsJobsVector.add("populateDimensionProduct");
            populateDimesionsJobsVector.add("populateDimensionBonus");
            populateDimesionsJobsVector.add("populateDimensionClosetState");
            populateDimesionsJobsVector.add("populateDimensionGarment");
            populateDimesionsJobsVector.add("populateDimensionVendor");
            populateDimesionsJobsVector.add("updateIntermediatePlansInfoInDimensionCustomer");
            populateDimesionsJobsVector.add("populateDimensionCustomer");

            populateDimesionsJobsVector.add("populateDimensionCustomerExtraRow1");
            populateDimesionsJobsVector.add("populateDimensionCustomerExtraRow2");
            populateDimesionsJobsVector.add("MappingGarmentSku");
            populateDimesionsJobsVector.add("assertCustomerTable");
            // Create view for recently downgraded plans.
            populateDimesionsJobsVector.add("populateRecentlyChangedPlansView");
            populateDimesionsJobsVector.add("populateDimensionMarketingChannels");
            populateDimesionsJobsVector.add("updateDimensionCustomer");
            populateDimesionsJobsVector.add("assertCustomerTable");
            populateDimesionsJobsVector.add("populateDimensionCustomerShortSession");
        }

        Vector<String> populateFactTablesJobsVector = new Vector<String>();
        populateFactTablesJobsVector.add("populateFactBraintreeTransactions");
        if (!QUICK_MODE) {
            populateFactTablesJobsVector.add("populateFactNewInventorySnapshot");
            populateFactTablesJobsVector.add("populateFactClosetSnapshot");
            populateFactTablesJobsVector.add("populateFactBraintreeSubscriptionsSnapshot");
            populateFactTablesJobsVector.add("populateFactClosetSnapshotDaily");
            populateFactTablesJobsVector.add("populateFactNewInventorySnapshotDaily");
            populateFactTablesJobsVector.add("populateFactBraintreeSubscriptionsSnapshotDaily");
            // Pending slots needs to be after closet and braintree snapshot.
            populateFactTablesJobsVector.add("populateFactUserPendingSlotSnapshot");
            populateFactTablesJobsVector.add("populateFactUserPendingSlotSnapshotDaily");

            // Create other views and derivatives.
            populateFactTablesJobsVector.add("populateFactShippingReport");
            populateFactTablesJobsVector.add("populateFactShippingReportDaily");

            // Unhealthy SKUs job.
            // populateFactTablesJobsVector.add("populateWtdOptimized");

            populateFactTablesJobsVector.add("populateFactProductMarketplace");

            // populateFactTablesJobsVector.add("compareCSVFiles");

            populateFactTablesJobsVector.add("populateFactProductMarketplaceDaily");

            // Upload Unhealth SKU list to S3.
            populateFactTablesJobsVector.add("dumpUnHealthySKUList");
            // job added to create FACT Rid Transactions.
            populateFactTablesJobsVector.add("populateFactRidTransactions");
            populateFactTablesJobsVector.add("populateFactInventoryTransaction");
            populateFactTablesJobsVector.add("populateFactSoldGarment"); // BI-1813
            populateFactTablesJobsVector.add("populateFactVendorReturn"); // BI-1829
            populateFactTablesJobsVector.add("populateFeedbackTransactions");
            // populateFactTablesJobsVector.add("populateGarmentRecovery");
            // populateFactTablesJobsVector.add("populateGarmentRecoveryPricing");
            // populateFactTablesJobsVector.add("populateZendeskTicketDetails");
            // populateFactTablesJobsVector.add("populateZendeskTags");
        }

        Vector<String> cleanupJobsVector = new Vector<String>();
        cleanupJobsVector.add("cleanupFactSnapshots");

        Vector<String> populateFeedbackJobVector = new Vector<String>();
        populateFeedbackJobVector.add("populateGarmentFeedback");

        Vector<String> dataStatisticsEnd = new Vector<String>();
        dataStatisticsEnd.add("populateTableStatistics");

        TalendProjectJob retrieveDumpProject = new TalendProjectJob("/home/gb/share/retrieve-dumps", retrieveDumpJobsVector);
        TalendProjectJob populateDimesnionProject = new TalendProjectJob("/home/gb/share/populate-dimensions", populateDimesionsJobsVector);
        TalendProjectJob populateFactTableProject =
                new TalendProjectJob("/home/gb/share/populate-fact-tables", populateFactTablesJobsVector);
        TalendProjectJob populateFeedbackProject = new TalendProjectJob("/home/gb/share/populate-feedback", populateFeedbackJobVector);

        TalendProjectJob cleanupProject = new TalendProjectJob("/home/gb/share/cleanup-tables", cleanupJobsVector);
        TalendProjectJob populateDataStatisticsStart = new TalendProjectJob("/home/gb/share/populate-fact-tables", dataStatisticsStart);
        TalendProjectJob populateDataStatisticsEnd = new TalendProjectJob("/home/gb/share/populate-fact-tables", dataStatisticsEnd);

        List<TalendProjectJob> talendProjectJobs = new ArrayList<TalendProjectJob>();
        talendProjectJobs.add(populateDataStatisticsStart);
        talendProjectJobs.add(retrieveDumpProject);
        talendProjectJobs.add(populateDimesnionProject);
        talendProjectJobs.add(populateFactTableProject);
        talendProjectJobs.add(populateFeedbackProject);
        talendProjectJobs.add(cleanupProject);
        talendProjectJobs.add(populateDataStatisticsEnd);

        // talendProjectJobs.add(testProject);
        this.setJobPipelineName(PIPELINE_NAME);

        this.init(BIConfig.getConfDir(), BIConfig.getJobName(), talendProjectJobs, ESTIMATED_COMPLETION_TIME);
    }
}
