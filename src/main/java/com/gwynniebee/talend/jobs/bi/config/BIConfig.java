/**
 * Copyright 2012 GwynnieBee Inc.
 */

package com.gwynniebee.talend.jobs.bi.config;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.IOException;
import java.util.Properties;

/**
 * @author Prashant TR Rao
 */
public final class BIConfig {

    /**
     * Job configuration.
     */
    private static final String PROPERTY_FILE = "/job.properties";

    /**
     * Log4j config location.
     */
    private static final String LOGGER_PROPERTIES = "/log4j.properties";

    /**
     * Properties.
     */
    private static Properties properties;

    /**
     * Initialized flag.
     */
    private static boolean initialized = false;

    /**
     * Private constructor.
     */
    private BIConfig() {
    }

    /**
     * Initialize logger.
     *
     * @throws java.io.IOException IOException
     */
    public static synchronized void init() throws IOException {
        if (initialized) {
            Logger log = Logger.getRootLogger();
            log.debug("Sample talend job already initialized");
            return;
        }

        PropertyConfigurator.configure(BIConfig.class.getResource(LOGGER_PROPERTIES));
        Logger log = Logger.getRootLogger();
        log.info("Sample Talend Job initialized");
        initialized = true;

        properties = new Properties();
        properties.load(BIConfig.class.getResourceAsStream(PROPERTY_FILE));
    }

    /**
     * Get the job name.
     * @return job name
     */
    public static String getJobName() {
        return properties.getProperty("job_name");
    }

    /**
     * Get job base directory.
     * @return base directory
     */
    public static String getBaseDir() {
        return properties.getProperty("job_base_dir");
    }

    /**
     * Get job conf directory.
     * @return conf directory
     */
    public static String getConfDir() {
        return properties.getProperty("job_conf_dir");
    }

}
